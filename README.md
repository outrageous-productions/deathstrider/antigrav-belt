# Antigrav Belt

An equipment addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

When powered, this belt will slightly increase your carry capacity. A high-power mode can be toggled that gives you low gravity.

Holding firemode and picking up an additional belt will upgrade it, increasing the carry capacity bonus at an additional energy cost.

---

See [credits.txt](./credits.txt) for attributions.
